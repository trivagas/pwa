module.exports = {
  theme: {
    extend: {
      height: {
        '7/10': '70vh',
        '4/10': '40vh'
      },
      inset: {
      	'1/2': '33%'
      }
    }
  },
  variants: {},
  plugins: []
}
