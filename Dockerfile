FROM node:alpine AS builder
ENV PORT=8080 \
    HOST=0.0.0.0
EXPOSE ${PORT}
WORKDIR /usr/src/app
COPY package.json yarn.lock ./

FROM builder AS development
RUN yarn install
COPY . ./
CMD yarn serve

FROM development AS base-production
RUN yarn build

FROM nginx:stable-alpine AS production
ENV PORT=8080
RUN mkdir /app
COPY --from=base-production /usr/src/app/dist /app
COPY nginx.conf.template /etc/nginx
CMD /bin/sh -c "envsubst '\${PORT}' < /etc/nginx/nginx.conf.template > /etc/nginx/nginx.conf && nginx -g 'daemon off;'"