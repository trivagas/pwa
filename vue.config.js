module.exports = {
  devServer: {
    disableHostCheck: true
  },
  chainWebpack: config => {
    config
      .plugin('define')
      .tap(args => {
          args[0] = {
             ...args[0],
             "VUE_APP_API_URL": JSON.stringify(process.env.VUE_APP_API_URL),
          }
          return args
       })
  }
}