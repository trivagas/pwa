export interface IOID {
  $oid: string;
}

export interface ILatLng {
  lat: number;
  lng: number;
}

export interface IMarker {
  _id: string;
  parkinglot_id: IOID;
  label: string;
  position: ILatLng;
}

export interface IPrice {
  order: number;
  label: string;
  price: string;
}

export interface IParkinglot {
  _id: IOID | null;
  name: string | null;
  address: string | null;
  openingHours: string | null;
  pricingTable: IPrice[] | null;
}