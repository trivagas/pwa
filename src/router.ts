import Vue from "vue";
import Router, { Route } from "vue-router";
import store from './store';

import Home from "@/views/Home.vue";
import Login from "@/views/Login.vue";
import Register from "@/views/Register.vue";
import Profile from "@/views/Profile.vue";
import Reservation from "@/views/Reservation.vue";
import Crowdsourcing from "@/views/Crowdsourcing.vue";
import Admin from "@/views/Admin.vue";

Vue.use(Router);

declare type beforeFilter = (to: Route, from: Route, next: Function) => void;

const authenticatedOrLogin: beforeFilter = (_: Route, __: Route, next: Function): void => {
  if (store.getters.isAuthenticated) {
    next();
  } else {
    next('/login');
  }
};

const isAdmin: beforeFilter = (_: Route, __: Route, next: Function): void => {
  const role: any = store.getters.getAccessTokens.role;
  console.log("PAoel");
  console.log(role);
  if (role == 'admin') {
    next();
  }
  else {
    next('/login');
  }
}

export default new Router({
  mode: "history",
  base: process.env.BASE_URL,
  routes: [
    {
      path: "/",
      name: "home",
      component: Home
    },
    {
      path: "/login",
      name: "login",
      component: Login
    },
    {
      path: "/register",
      name: "register",
      component: Register
    },
    {
      path: "/profile",
      name: "profile",
      component: Profile,
      beforeEnter: authenticatedOrLogin,
    },
    {
      path: "/reservation",
      name: "reservation",
      component: Reservation,
      beforeEnter: authenticatedOrLogin,
    },
    {
      path: '/newParkinglot',
      name: 'crowdsourcing',
      component: Crowdsourcing,
      beforeEnter: authenticatedOrLogin,
    },
    {
      path: '/administration',
      name: 'administration',
      component: Admin,
      beforeEnter: authenticatedOrLogin,
    }
  ]
});
