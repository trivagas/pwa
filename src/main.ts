import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import axios from 'axios';
import VueAxios from 'vue-axios';
import "./registerServiceWorker";

import Vuetify from 'vuetify'
import "@/assets/css/tailwind.css";
import 'vuetify/dist/vuetify.min.css'

Vue.config.productionTip = false;
Vue.use(Vuetify);
Vue.use(VueAxios, axios);


axios.defaults.baseURL = process.env.VUE_APP_API_URL;
axios.defaults.headers.common = {
  ...axios.defaults.headers.common,
  'Access-Control-Allow-Origin': '*',
  'Access-Control-Allow-Methods': 'GET, POST, PUT, PATCH, DELETE, OPTIONS',
  'Access-Control-Allow-Headers': 'Origin, Content-Type, X-Auth-Token',
}

new Vue({
  router,
  store,
  vuetify: new Vuetify(),
  render: h => h(App)
}).$mount("#app");
