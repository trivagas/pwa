import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    selectedTimePicker: false,
    selectedParkinglot_id: null,
    reservation: {
      arrive: null,
      leave: null,
    },
    user: {
      token: null,
      refresh: null,
      id: null,
      role: null
    }
  },
  getters: {
    isParkinglotSelected(st: any): boolean {
      return st.selectedParkinglot_id !== null;
    },
    isTimePickerSelected(st: any): boolean {
      return st.selectedTimePicker;
    },
    selectedParkinglot_id(st: any): string {
      return st.selectedParkinglot_id['$oid'];
    },
    isAuthenticated(st: any): boolean {
      return st.user.token !== null;
    },
    getAccessTokens(st: any): any {
      return st.user;
    },
    getReservation(st: any): any {
      return st.reservation;
    }
  },
  mutations: {
    selectParkinglot(st: any, parkinglot: any) {
      st.selectedParkinglot_id = parkinglot;
    },
    selectTimePicker(st: any) {
      st.selectedTimePicker = true;
    },
    setReservation(st: any, reservation: any) {
      st.reservation.arrive = reservation.arrive
      st.reservation.leave = reservation.leave
    },
    clearSelectedParkinglot(st: any): void {
      st.selectedParkinglot_id = null;
    },
    clearTimePicker(st: any): void {
      st.selectedTimePicker = false;
    },
    login(st: any, { token, refresh, id, role}: any) {
      st.user.token = token;
      st.user.refresh = refresh;
      st.user.id = id;
      st.user.role = role;
    }
  },
  actions: {
    selectParkinglot({ commit }: any, parkinglot_id: string) {
      commit('selectParkinglot', parkinglot_id);
    },
    selectTimePicker({ commit }: any) {
      commit('selectTimePicker');
    },
    setReservation({ commit }: any, reservation: any) {
      commit('selectTimePicker', reservation);
    },
    clearSelectedParkinglot({ commit }: any) {
      commit('clearSelectedParkinglot');
    },
    clearTimePicker({ commit }: any) {
      commit('clearTimePicker');
    },
    login({ commit }: any, payload: any) {
      commit('login', payload);
    }
  }
});
